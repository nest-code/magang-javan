
CREATE TABLE role (
    id BIGINT (3) PRIMARY KEY,
    title TEXT UNIQUE NOT NULL
);


INSERT INTO role(id, title)
VALUES
       (1, 'Direktur'),
       (2, 'Manajer'),
       (3, 'Staff');


ALTER TABLE karyawan ADD COLUMN role INT REFERENCES role(id) ON DELETE RESTRICT ON UPDATE CASCADE;


UPDATE karyawan SET role=1 WHERE departemen=1 AND nama='Rizki Saputra';
UPDATE karyawan SET role=2 WHERE departemen=1 AND nama IN ('Farhan Reza', 'Riyando Adi');
UPDATE karyawan SET role=3 WHERE departemen NOT IN (1) AND nama NOT IN ('Rizki Saputra', 'Farhan Reza', 'Riyando Adi');


ALTER TABLE karyawan ALTER COLUMN role SET NOT NULL;


SELECT id, nama FROM karyawan WHERE role > 2 ORDER BY id;