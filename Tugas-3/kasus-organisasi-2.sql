CREATE TABLE company (
    id BIGINT (2) PRIMARY KEY,
    nama TEXT UNIQUE NOT NULL,
    alamat TEXT NOT NULL
);

CREATE TABLE employee (
    id BIGINT (5) PRIMARY KEY,
    nama TEXT UNIQUE NOT NULL,
    atasan_id INT REFERENCES employee(id),
    company_id INT NOT NULL REFERENCES company(id)
);

INSERT INTO company(id, nama, alamat)
VALUES
    (1, 'PT JAVAN', 'Sleman'),
    (2, 'PT Dicoding', 'Bandung');

INSERT INTO employee(id, nama, atasan_id, company_id)
VALUES
    (1, 'Pak Budi', null, 1),
    (2, 'Pak Tono', 1, 1),
    (3, 'Pak Totok', 1, 1),
    (4, 'Bu Sinta', 2, 1),
    (5, 'Bu Novi', 3, 1),
    (6, 'Andre', 4, 1),
    (7, 'Dono', 4, 1),
    (8, 'Ismir', 5, 1),
    (9, 'Anto', 5, 1);


-- Select CEO
SELECT * FROM employee WHERE atasan_id IS NULL;

-- Select Staff
SELECT * FROM employee
EXCEPT
    (SELECT DISTINCT a.* FROM employee a JOIN employee b ON a.id = b.atasan_id)
ORDER BY id;

-- Select Direktur
SELECT * FROM employee
WHERE atasan_id =
    (SELECT id FROM employee WHERE atasan_id IS NULL);

-- Select Manager
SELECT * FROM employee
WHERE atasan_id IN
    (SELECT id FROM employee WHERE atasan_id = (SELECT id FROM employee WHERE atasan_id IS NULL));

-- Jumlah bawahan
WITH RECURSIVE bawahan AS (
    SELECT id, nama FROM employee WHERE nama='Pak Budi'
    UNION
    SELECT a.id, a.nama FROM employee a JOIN bawahan b ON b.id = a.atasan_id
) SELECT COUNT(id) as jumlah_bawahan FROM bawahan WHERE nama != 'Pak Budi';
