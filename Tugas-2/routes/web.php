<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('tugas-2/index');
});

Route::get('/hello', function () {
    return view('tugas-2/hello');
});

Route::get('/oddeven', 'OddevenController@index');
Route::post('/oddevenprocess', 'OddevenController@process');


Route::get('/vovel', 'VovelController@index');
Route::post('/vovelprocess','VovelController@process');

Route::get('/calculator', 'CalculatorController@index');
Route::post('/calculatorprocess','CalculatorController@process');



