<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Calculator;


class CalculatorController extends Controller
{
    public function index()
    {
       return view('tugas-2/calculator');
    }

    public function process(Request $request)
    {
        $angka1 = $request->input('angka1');
        $angka2 = $request->input('angka2');

        $operator = $request->input('operator');
        $result = 0;

        if ($operator == '+') {
            $result = $angka1 + $angka2;
        }elseif ($operator == '-') {
            $result = $angka1 - $angka2;
        }elseif($operator=='*'){
            $result = $angka1 * $angka2;
        }elseif ($operator=='/') {
            $result = $angka1 / $angka2;
        }elseif ($operator=='%') {
            $result = $angka1 % $angka2;
        }else{
            $result = 0;
        }

        return redirect('/calculator')->with('message','Hasil : '.$result);
    }


}
