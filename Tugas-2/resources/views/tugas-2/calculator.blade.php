@extends('main')

@section('title', 'Kalkulator Sederhana')

@section('content')
<div class="row">
    <div class="mx-auto">
        <div class="card" style="width: 20rem;">
            <div class="card-body">
                <form action="{{url('calculatorprocess')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Angka 1</label>
                        <input type="number" name="angka1" class="form-control" min="1" required>
                    </div>

                    <div class="form-group">
                        <label>Angka 2</label>
                        <input type="number" name="angka2" class="form-control" min="1" required>
                    </div>

                    <div class="form-group">
                        <label>Operator</label>
                        <select name="operator" class="form-control" required>
                            <option value="+">+</option>
                            <option value="-">-</option>
                            <option value="*">*</option>
                            <option value="/">/</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <a href="{{url('/')}}" class="btn btn-default">Kembali</a>
                        <button type="submit" class="btn btn-primary">Hasil</button>
                    </div>
                </form>
            </div>
        </div>


        @if(session('message'))
        <div class="alert alert-warning">
            <h1 class="text-center">{{ session('message') }}</h1>
        </div>
        @endif

    </div>
</div>



@endsection