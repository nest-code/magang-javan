<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Selamat Datang</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito';
            justify-content: center;
            align-items: center;
            display: flex;

        }
        h1
        {
            font-size: 100px;
            color: #a0aec0;
            text-shadow: 2px 2px #1a202c;
        }
        a{
            margin:10px;
            color:black;
            text-decoration: none;
        }
        .hello{
            backgorund-color:black;
        }
    </style>
</head>
<body>
<a href="{{url('hello')}}">Hello</a>
<a href="{{url('calculator')}}">Kalkulator</a>
<a href="{{url('oddeven')}}">Ganjil Genap</a>
<a href="{{url('vovel')}}">Hitung Huruf Vokal</a>
</body>
</html>
